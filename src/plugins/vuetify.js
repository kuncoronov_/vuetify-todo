import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

const opt = {
	icons: {
		iconfont: 'mdi',
	},
	theme: {
		themes: {
			light: {
				primary: '#EF959D',
				secondary: '#424242',
				accent: '#68233D',
				error: '#EF3B49',
				info: '#3DA5D1',
				success: '#D2CD4B',
				warning: '#FC9E4B',
			},
		},
	},
}

export default new Vuetify(opt)
